package com.example.training.tradematching;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradeMatchingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeMatchingApplication.class, args);
	}

}
